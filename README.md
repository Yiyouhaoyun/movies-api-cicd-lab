# agile ca2 movies-api-cicd
Name: Yangqing Li

## API EndPoints

### Users EndPoint

- GET /api/users   Retrieves a list of users.
- POST /api/users  Used for creating a new user.
- GET /api/user/relevant/movies  Retrieves movies relevant to the current user.
- POST /movies API Tests  Adds a new movie to the system.
- DELETE /api/user/relevant/movies (Auth) Removes a movie from the user's relevant list.
- GET /api/user/relevant/actors (Auth) Retrieves actors relevant to the current user.
- POST /api/user/relevant/actors (Auth) Adds an actor to the user's relevant list.
- DELETE /api/user/relevant/actors (Auth) Removes an actor from the user's relevant list.
- GET /api/user/relevant/toWatch (Auth) Retrieves the user's "to watch" list of movies.
- POST /api/user/relevant/toWatch (Auth) Adds a movie to the user's "to watch" list.
- DELETE /api/user/relevant/toWatch (Auth) Removes a movie from the user's "to watch" list.

### Movies EndPoint

- GET /api/movies Retrieves a list of movies from your database.
- GET /api/movies/:id Fetches details of a specific movie by its ID.
#### GET TMDB movies
- GET /api/movies/tmdb Retrieves a list of movies from TMDB.
- GET /api/movies/tmdb/movie/:id Fetches details of a specific TMDB movie by its ID.
- GET /api/movies/tmdb/upcoming Retrieves a list of upcoming movies from TMDB.
- GET /api/movies/tmdb/popularMovies Fetches a list of popular movies from TMDB.
- GET /api/movies/tmdb/genres Retrieves a list of movie genres from TMDB.
- GET /api/movies/tmdb/:id/reviews Fetches reviews for a specific TMDB movie.
- GET /api/movies/tmdb/trendingMovie Retrieves trending movies from TMDB.
- GET /api/movies/tmdb/:id/credits Fetches the credits (cast and crew) for a specific TMDB movie.

### Actors EndPoint
#### GET TMDB actors
- GET /api/actors/tmdb Retrieves a list of actors from TMDB.
- GET /api/actors/tmdb/:id Fetches details of a specific actor from TMDB by their ID.
- GET /api/actors/tmdb/:id/credits Retrieves the credits (movies and TV shows participated in) for a specific actor from TMDB.

### Shows EndPoint
- GET /api/shows/tmdb/tvshows Retrieves a list of TV shows from TMDB.
- GET /api/shows/tmdb/tvshows/:id Fetches details of a specific TV show from TMDB by its ID.


## Test
![user test](image/test1.png)
![user test](image/test2.png)
![movies test](image/test3.png)
![actors test](image/test4.png)
![tvshow test](image/test5.png)
![tvshow test](image/test6.png)
![test result](image/test7.png)

## Deploments

https://movies-api-staging-yangqingli-790d34673e97.herokuapp.com/api/movies/tmdb/movies

https://movies-api-staging-yangqingli-790d34673e97.herokuapp.com/api/movies

## Independent Learning 
![Independent learning](image/independent%20study.png)

I tried many times but couldn't make it to the official website, so I only finished reporting code coverage after local testing.