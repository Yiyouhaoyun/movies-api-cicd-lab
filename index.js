import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import usersRouter from './api/users';
import './db';
import defaultErrHandler from './errHandler';
import moviesRouter from './api/movies';   //import movies router
import authenticate from './authenticate';
import userRelevant from "./api/userRelevant";
import actorsRouter from "./api/actors";
import showsRouter from "./api/shows";
import './seedData';



dotenv.config();

const app = express();
const port = process.env.PORT; 

app.use(cors());
app.use(express.json());
app.use('/api/users', usersRouter);
app.use('/api/movies', moviesRouter); //ADD THIS BEFORE THE DEFAULT ERROR HANDLER.
app.use('/api/user/relevant', authenticate, userRelevant);
app.use('/api/actors', actorsRouter);
app.use('/api/shows', showsRouter);
app.use(defaultErrHandler);


let server = app.listen(port, () => {
  console.info(`Server running at ${port}`);
  });
  module.exports = server